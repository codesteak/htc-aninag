<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aninag extends Aninag_Controller {

	/**
	* \brief The class constructor
	*
	* Calls the parent constructor.
	*/
	public function __construct() 
	{
		parent::__construct();
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// echo("Hey!");
		// die();
		// $this->load->view('highmaps');
		# load html with general view maps.
		# JS responsibility: paabutin until city
		# onclick ng city: call snapshot(palce = "")
		$this->load_view('index');
	}

	public function world() {
		$this->load_view('highmaps');	
	}
	public function gmaps_snapshot() {
		# load view: picture

		# querystring: maps.google.com?pace="asdfghj"
		# <img src="">

		# original
		// echo($place); die();
		$place = $_POST['city'];

		# SAMPLE: data
		# Array ( [city] => Sakha (Yakutia) )

		$this->load_view('gmaps', compact('place'));
	}

	public function nature_timeline() {
		# to be triggered by the slider
		# preprocessing depending on years

		$this->load->view('nature_timeline');
	}

	public function image_processing()
	{
		$this->load->view('image_processing');
	}
}
