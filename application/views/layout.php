<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<title>Welcome to Aninag!</title>
		<link rel="stylesheet" type="text/css" href="/htc-aninag/stylesheets/main.css">
		<link rel="stylesheet" type="text/css" href="/htc-aninag/stylesheets/bootstrap.min.css">
    <script type="text/javascript" src="/htc-aninag/javascripts/jquery-2.1.4.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Nothing+You+Could+Do' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete"
         async defer></script>
		<script type="text/javascript" src="/htc-aninag/javascripts/main.js"></script>
		<link type="text/css" href="/htc-aninag/stylesheets/carousel.css" rel="stylesheet">
    <style>
      .cursive {
        font-family: 'Nothing You Could Do', cursive;
        font-weight: 900;
      }

      .header-logo {
        font-size: 32px;
      }
    </style>
	</head>
  <body>
    <? if($view != 'index') { ?>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand cursive header-logo" href="/htc-aninag">Aninag</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <!-- <form class="navbar-form navbar-right">
              <div class="form-group">
                <input type="text" placeholder="Email" class="form-control">
              </div>
              <div class="form-group">
                <input type="password" placeholder="Password" class="form-control">
              </div>
              <button type="submit" class="btn btn-success">Sign in</button>
            </form> -->
            <!-- <div class="navbar-form navbar-right">
              <button type="submit" class="btn btn-default">Sign in</button>
              <button type="submit" class="btn btn-default">Donate</button>
            </div> -->
          </div>
        </div>
      </nav>
    <? } ?>
    <? $this->load->view($view, $vars) ?> 
  </body>
</html>