<html>
        <head>
                <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
                <meta charset="utf-8">
                <title>Welcome to Aninag!</title>
                <link rel="stylesheet" type="text/css" href="/htc-aninag/stylesheets/main.css">
                <script type="text/javascript" src="/htc-aninag/javascripts/jquery-2.1.4.js"></script>
                <script type="text/javascript" src="/htc-aninag/javascripts/main.js"></script>
                <link rel="stylesheet" type="text/css" href="/htc-aninag/stylesheets/dragdealer.css">
                <script type="text/javascript" src="/htc-aninag/javascripts/dragdealer.js"></script>

                <script>
                var map = document.getElementById("map");
                var canvas = document.createElement("canvas");
                var ctx = canvas.getContext("2d");
                var originalPixels = null;
                var currentPixels = null;

                function getPixels(img)
                {
                    canvas.width = img.width;
                    canvas.height = img.height;

                    ctx.drawImage(img, 0, 0, img.naturalWidth, img.naturalHeight, 0, 0, img.width, img.height);
                    originalPixels = ctx.getImageData(0, 0, img.width, img.height);
                    currentPixels = ctx.getImageData(0, 0, img.width, img.height);

                    img.onload = null;
                }

                function changeColor(degree)
                {
                    var threshold = 255.0 - (255.0*(degree/100.0));
                    if(!originalPixels) return; // Check if image has loaded
                    for(var I = 0, L = originalPixels.data.length; I < L; I += 4)
                    {
                        if(currentPixels.data[I + 3] > 0)
                        {
                            if (Math.max(originalPixels.data[I], originalPixels.data[I + 1], originalPixels.data[I + 2]) >= threshold) {
                                var greenPeg = originalPixels.data[I]/2.0 + 0.0;
                                var greenDecrement = originalPixels.data[I + 1] - greenPeg;
                                currentPixels.data[I + 1] = (originalPixels.data[I + 1] - (greenDecrement*(degree/100.0))) - 0.0;
                                currentPixels.data[I + 2] = originalPixels.data[I + 2] - (originalPixels.data[I + 2]*(degree/100.0));
                            }
                        }
                    }
                    ctx.putImageData(currentPixels, 0, 0);
                    document.getElementById("map").src = canvas.toDataURL("image/png");
                }

                function increaseTime() {
                    var time = $( "#slider" ).slider( "value" );
                    changeColor(time);
                }

                $(function() {
                    new Dragdealer('slider-handler', {
                      animationCallback: function(x, y) {
                        $('#slider .value').text(Math.round(x * 100));
                      }
                    });
                });

                // $(function() {
                //     $( "#slider" ).slider({
                //       orientation: "horizontal",
                //       max: 100,
                //       value: 0,
                //       step: 5,
                //       slide: increaseTime,
                //       change: increaseTime
                //     });
                // });
                </script>

        </head>
  <body align="center">
    <br/>
    <img src="/img/maps.png" id="map" height="300" width"120" onload="getPixels(this)"></img>
    <br/><br/>
    <div id="slider" style="width:300px; margin:auto"></div>
    <br/>
  </body>
</html>