    <style>
    	.featurez {
    		background-color: rgba(255,255,255,0.7);
    		border-radius: 25px;
    		padding-left: 36px;
    		padding-right: 36px;
    	}
    </style>
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="/htc-aninag/images/jumbo2.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 class="cursive front-logo" style="font-size:150px">Aninag</h1>
              <h1>Your glimpse of the environment's future.</h1>
              <br/>
              <!-- <h4>You are currently in Makati City, Philippines.</h4> -->
              <p><a class="btn btn-lg btn-primary" href="Aninag/world" role="button">Gain Insights Now</a></p>
              <br/>
              <br/>
            </div>
          </div>
        </div>

        <div class="item">
          <img class="first-slide" src="/htc-aninag/images/hero_city.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 class="cursive front-logo" style="font-size:90px">
                <span>Aninag:
                </span>
                <span style="text-decoration: underline;">Predict</span>
              </h1>
              <h1>Determine an area's sustainability</h1>
              <br/>
              <!-- <h4>You are currently in Makati City, Philippines.</h4> -->
              <p><a class="btn btn-lg btn-primary" href="Aninag/world" role="button">Gain Insights Now</a></p>
              <br/>
              <br/>
            </div>
          </div>
        </div>

        <div class="item">
          <img class="first-slide" src="/htc-aninag/images/hero_visualize.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 class="cursive front-logo" style="font-size:90px">
                <span>Aninag:
                </span>
                <span style="text-decoration: underline;">Visualize</span>
              </h1>
              <h1>Simplified information from advanced image processing techniques</h1>
              <br/>
              <!-- <h4>You are currently in Makati City, Philippines.</h4> -->
              <p><a class="btn btn-lg btn-primary" href="Aninag/world" role="button">Gain Insights Now</a></p>
              <br/>
              <br/>
            </div>
          </div>
        </div>

        <div class="item">
          <img class="first-slide" src="/htc-aninag/images/hero_act.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 class="cursive front-logo" style="font-size:90px">
                <span>Aninag:
                </span>
                <span style="text-decoration: underline;">Act</span>
              </h1>
              <h1>Act now and get involved before it's too late</h1>
              <br/>
              <!-- <h4>You are currently in Makati City, Philippines.</h4> -->
              <p><a class="btn btn-lg btn-primary" href="Aninag/world" role="button">Gain Insights Now</a></p>
              <br/>
              <br/>
            </div>
          </div>
        </div>


      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <div class="img-circle" style="min-height: 140px;">
            <img src="/htc-aninag/images/visualize.png" alt="Generic placeholder image" height="130" style="margin-top: -20px;">
          </div>
          <h2>Predict</h2>
          <p align="justify">Using aggregated data from various API sources and verified crowd-sourced information, we determine the approximate sustainability of an area given its current conditions to give us a glimpse of the future given our current standards of living.</p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <div class="img-circle" style="min-height: 140px">
            <img src="/htc-aninag/images/predict.png" alt="Generic placeholder image" width="140" height="140">
          </div>
          <h2>Visualize</h2>
          <p align="justify">Data are presented in an easily-digested manner so people from all walks of life can participate. Environment degradation and resource depletion are then rendered using advanced image processing technologies to further deliver the message across.</p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <div class="img-circle" style="min-height: 140px;">
            <img src="/htc-aninag/images/act.png" alt="Generic placeholder image" height="110">
          </div>
          <h2>Act</h2>
          <p align="justify">These information will raise awareness of the general public and can then be leveraged by the government, NGOs, and even individuals to make conscious efforts so the we can act before it is too late.</p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

	  <div align="center">
      <h1>Features</h1>
      <hr/>
  	  </div>

      <div class="row featurette" style="background-image:url('/htc-aninag/images/crowd.jpg'); background-size:100%; background-position: 0px -150px;  min-height: 350px;">
        <div class="col-md-7">
          <div class="featurez">
          	<h2 class="featurette-heading">Crowd-sourced and Verified<span class="text-muted"> Information</span></h2>
          	<p class="lead">Get our tied-up app now. Crowd sourcing in form of surveys to indirectly get approximate data from users. Get the latest tips of the day.</p>
          </div>
        </div>
      </div>
      <br/>
      <div class="row featurette" style="background-image:url('/htc-aninag/images/maps.jpg'); background-size:100%; min-height: 350px;">
        <div class="col-md-7 col-md-push-5">
        	<div class="featurez">
          	<h2 class="featurette-heading">Advanced<span class="text-muted"> Predictive Modelling</span></h2>
          	<p class="lead">Algorithms becoming more accurate and intelligent over time. People contributing more and new important decision parameters to be considered and inferred from.</p>
        	</div>
        </div>
      </div>
      <br/>
      <div class="row featurette" style="background-image:url('/htc-aninag/images/data_desk.jpg'); background-size:100%; background-position: 0px -150px; min-height: 350px;">
        <div class="col-md-7">
        	<div class="featurez">
         	 <h2 class="featurette-heading">Graphs and<span class="text-muted"> Analytics</span></h2>
         	 <p class="lead">Data aggregated to graphs and charts that are easy to digest and analyze. Now, anybody can have access to simplified yet insightful data.</p>
        	</div>
        </div>
      </div>
      <br/>
      <div class="row featurette" style="background-image:url('/htc-aninag/images/machine.jpg'); background-size:100%; background-position: 0px -130px; min-height: 350px;">
        <div class="col-md-7 col-md-push-5">
        	<div class="featurez">
          	<h2 class="featurette-heading">Extensive<span class="text-muted"> API Support</span></h2>
          	<p class="lead">Our APIs open up a world of possibilities for integration to interested third party developers to be used in your favorite app, service, or yet another platform.</p>
      		</div>
        </div>
      </div>
      <br/>
      <div class="row featurette" style="background-image:url('/htc-aninag/images/text_message.jpg'); background-size:100%; background-position: 0px -150px; min-height: 350px;">
        <div class="col-md-7">
        	<div class="featurez">
          	<h2 class="featurette-heading">SMS<span class="text-muted"> Messaging</span></h2>
          	<p class="lead">Take part in the efforts against Climate Change anytime, anywhere, even without data connection.</p>
        	</div>
        </div>
      </div>
      <br/>
      <div class="row featurette" style="background-image:url('/htc-aninag/images/social_media.jpg'); background-size:100%; background-position: -100px -280px; min-height: 350px;">
        <div class="col-md-7 col-md-push-5">
        	<div class="featurez">
          	<h2 class="featurette-heading">Social Media<span class="text-muted"> Integration</span></h2>
          	<p class="lead">Social media opens up a lot of opportunities in spreading our message as eco-hacktivists and connecting to a wider audience.</p>
        	</div>
        </div>
    </div>

  <!-- /END THE FEATURETTES -->
  <br/>

  <!-- FOOTER -->
  <footer>
    <p class="pull-right"><a href="#">Back to top</a></p>
    <p>&copy; 2014 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
  </footer>

</div>
