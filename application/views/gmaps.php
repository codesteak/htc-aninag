<?php //The name of this file in this example is imgdata.php
	copy("https://maps.googleapis.com/maps/api/staticmap?center=".urlencode($place)."&size=800x600&maptype=hybrid", "images/".$place.".png");
?>

<script type="text/javascript" src="/htc-aninag/javascripts/dragdealer.js"></script> 
<link rel="stylesheet" type="text/css" href="/htc-aninag/stylesheets/dragdealer.css"/>
<style>
html {
	width: 1300px;
	height: 800px;
	overflow: auto;
}

body {
	padding-bottom: 0px;
}

.map-image {
	height: 550px;
	top: 2%;
	position: relative;
	margin: auto;
	display: block;
	border-radius: 300px;
	border: 5px solid #FFF;
	position: absolute;
    left: 350px;
    top: 80px;
    box-shadow: 6px 6px 6px #111;
}

.stats {
	/*border: 2px solid red;*/
	height: 100%;
	display: none;
	float: left;
	background-color: #FFFFFF !important;
	padding: 10px;
}

.map {
	/*border: 2px solid blue;*/
	height: 100%;
	width: 100%;
	float: right;
	background-image: url('https://maps.googleapis.com/maps/api/staticmap?center=<?=$place?>&size=800x600&maptype=satellite&scale=3');
	background-repeat: none;
	background-size: 100% 100%;
	overflow-x: auto;
	opacity: 0.9;

	-webkit-filter: blur(2px);
	-moz-filter: blur(5px);
	-o-filter: blur(5px);
	-ms-filter: blur(5px);
	filter: blur(5px);
}

.big-round-button {
	position: absolute;
	border-radius: 64px;
	padding: 10px;
	width: 128px;
	height: 128px;
	cursor: pointer;
	/*box-shadow: 2px 2px 2px #111;*/
	display: none;
}

.big-round-button > img {
	left: 5px;
    position: relative;
    top: 5px;
    width: 100px;
}

#a {
	left: 200px;
	top: 100px;
	background-color: #CF000F;
}

#b {
	left: 100px;
	top: 280px;
	background-color: #F9690E;
}

#c {
	left: 200px;
	top: 450px;
	background-color: #663399;
}

#d {
	left: 1000px;
	top: 100px;
	background-color: #26A65B;
}

#e {
	left: 1000px;
	top: 450px;
	background-color: #4183D7;
}

#f {
	left: 1100px;
	top: 280px;
	background-color: #DB0A5B;
}

.dragger{
	width: 600px;
	border: 1px solid #CACACA;
	border-radius: 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	padding-top: 5px;
	padding-left: 5px;
	color: #ABABAB;
}
/*.dragger > .slider{
	background-color: #555;
	width: 70px;
	border-radius: 5px 5px 5px 5px;
	border-right: 0px;
}
*/
.hp-bar-container{
	position: absolute;
	left: 0px; top: 0px;
	height: 100%;
	background-color: rgba(0,95,185, 0.2); /*OUTSIDE*/
	padding: 0px;
}
.ua-point-slider > .dragdealer > .hp-bar-container{
	border-radius: 5px 5px 5px 5px;
	-moz-border-radius: 5px 5px 5px 5px;
	-webkit-border-radius: 5px 5px 5px 5px;
}
.hp-bar{
	position: absolute;
	left: 0px; top: 0px;
	width: 100%;
	height: 100%;
	background-color: rgba(38, 166, 91, 0.5); /*INSIDE*/
}
.dragdealer > .slider-bg > .slider{
	background-color: rgba(217, 30, 24, 1); /*INSIDE*/
	/*width: 100%;*/
	color: #FFFFFF;
	height: 30px;
	width: 70px;
	/*vertical-align: center;*/
	border-radius: 5px 5px 5px 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	padding-left: 10px;
	padding-top: 5px;
    position: relative;
}
.dragdealer > .slider-bg{
	border-radius: 5px 5px 5px 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	background-color: #26A65B; /*OUTSIDE*/
	top: -1px;
	padding: 0px;
}
.dragdealer > .slider-text{
	text-align: right;
}
.dragdealer {
	top: 570px;
	left: 335px;
}
.yay {
	height: 550px;
	top: 2%;
	position: relative;
	margin: auto;
	border-radius: 300px;
	border: 5px solid rgba(0,0,0,0);
	position: absolute;
    left: 350px;
    top: 80px;
    box-shadow: 6px 6px 6px #111;
    opacity: 0.5;
    display: none;
    text-align: center;
}
.gitna {
	height: 50px;
	width: 500px;
	font-size: 30pt;
	text-align: center;

	position: absolute;
	top: 150px;
	left: 400px;
	color: white;
}
.gitna-description {
	text-align: center;

	position: absolute;
	top: 250px;
	left: 400px;
	color: white;

	font-size: 40pt;
	text-align: center;
	height: 250px;
	width: 500px;
}
</style>
<script type="text/javascript">
	var map;
	var canvas;
	var ctx;
	var originalPixels;
	var currentPixels;
	var previousDegree = 0;

	function pollute(type, element){
		var title, description;
		var randNum = Math.random(); // 0 to 1
		// var widthPercent = $(".hp-bar-container").width() / $('.hp-bar-container').parent().width() * 100;

		if (type == 1){
			title = "Air Pollution";
			description = "<b style='font-size:120pt' font-family: \"Impact\";>" + parseFloat(randNum).toFixed(2) + "</b><br/>ppm";
		} else if (type == 2) {
			title = "Carbon Footprint";
			description = "<b style='font-size:120pt' font-family: \"Impact\";>" + parseFloat(randNum*8).toFixed(2) + "</b><br/>ppm";
		} else if (type == 3) {
			title = "Population";
			description = "<b style='font-size:120pt' font-family: \"Impact\";>" + parseFloat(randNum*10).toFixed(2) + "</b><br/>million";
		} else if (type == 4) {
			title = "Forest Cover";
			description = "<b style='font-size:120pt' font-family: \"Impact\";>" + parseFloat(randNum*5).toFixed(2) + "</b><br/>hectares";
		} else if (type == 5) {
			title = "Water Pollution";
			description = "<b style='font-size:120pt' font-family: \"Impact\";>" + parseFloat(randNum).toFixed(2) + "</b><br/>ppm";
		} else {
			title = "Wildlife";
			description = "<b style='font-size:120pt' font-family: \"Impact\";>" + parseFloat(randNum).toFixed(2) + "</b><br/>thousand species";
		}
		// alert(str);

		$('.yay').fadeIn(200);
		$('.gitna').fadeIn(200);
		$('.gitna-description').fadeIn(200);
		var bgColor = $(element).css('background-color');
		// $('.yay').fadeIn(200);

		$('.yay').width($('.map-image').width());
		// alert();
		$('.yay').css('background-color', bgColor);
		$('.gitna').html(title);
		$('.gitna-description').html(description);
		// alert($('.gitna-description').attr('class'));
		// alert($('.gitna-description').html());
	}

    function getPixels(img)
    {
    	img.crossOrigin = "anonymous";
    	// alert("Got pixels!");
        canvas.width = img.width;
        canvas.height = img.height;

        ctx.drawImage(img, 0, 0, img.naturalWidth, img.naturalHeight, 0, 0, img.width, img.height);
        originalPixels = ctx.getImageData(0, 0, img.width, img.height);
        currentPixels = ctx.getImageData(0, 0, img.width, img.height);

        img.onload = null;
    }

    function changeColor(degree)
    {
        var threshold = 255.0 - (255.0*(degree/100.0));
        if(!originalPixels) return; // Check if image has loaded
        for(var I = 0, L = originalPixels.data.length; I < L; I += 4)
        {
			if(currentPixels.data[I + 3] > 0)
            {
            	var greenPeg = originalPixels.data[I]/2.0;
	           	var greenDecrement = originalPixels.data[I + 1] - greenPeg;
            	if (degree >= previousDegree) {
	                if (Math.max(originalPixels.data[I], originalPixels.data[I + 1], originalPixels.data[I + 2]) >= threshold) {
	                    currentPixels.data[I + 1] = (originalPixels.data[I + 1] - (greenDecrement*(degree/100.0)));
	                    currentPixels.data[I + 2] = originalPixels.data[I + 2] - (originalPixels.data[I + 2]*(degree/100.0));
	                }
            	}	
            	else 
            	{	
            		if (Math.max(originalPixels.data[I], originalPixels.data[I + 1], originalPixels.data[I + 2]) < threshold) {
	                    currentPixels.data[I + 1] = (originalPixels.data[I + 1] + (greenDecrement*(degree/100.0)));
	                    currentPixels.data[I + 2] = originalPixels.data[I + 2] + (originalPixels.data[I + 2]*(degree/100.0));
	                }
            	}
            }
        }
        previousDegree = degree;
        ctx.putImageData(currentPixels, 0, 0);
        document.getElementById("map").src = canvas.toDataURL("image/png");
    }

    function increaseTime() {
        var time = $( "#slider" ).slider( "value" ); // 0 to 100
        changeColor(time);
    }


                // $(function() {
                //     $( "#slider" ).slider({
                //       orientation: "horizontal",
                //       max: 100,
                //       value: 0,
                //       step: 5,
                //       slide: increaseTime,
                //       change: increaseTime
                //     });
                // });
                
	$(document).ready(function(){
		$('.map-image').hover(function(){
			$('.big-round-button').fadeIn(200);
		});

		$('.big-round-button').hover(function(){
			pollute($(this).attr('type'), $(this));
		}, function(){
			$('.yay').fadeOut(200);
			$('.gitna').fadeOut(200);
			$('.gitna-description').fadeOut(200);
		});

		$('#navbar').html("<?=$place?>");
		$('#navbar').css('color', '#DEDEDE');
		$('#navbar').css('text-align', 'center');
		$('#navbar').css('font-size', '25px');
		$('#navbar').css('padding-top', '15px');
		$('#navbar').css('position', 'relative');
		$('#navbar').css('left', '120px');
		$('#navbar').css('width', '739px');

		var mainMap = $('.map-image');
		$('.yay').width($('.map-image').width());

		map = document.getElementById("map");
	    canvas = document.createElement("canvas");
	    ctx = canvas.getContext("2d");
	    originalPixels = null;
	    currentPixels = null;

		// new Dragdealer('demo-simple-slider');
		rewardDealer = new Dragdealer('just-a-slider', {
		  animationCallback: function(x, y) {
			var sliderText = Math.round((x*100)+2015);
			$('#just-a-slider > .handle > .value').text(sliderText);
			$('#AchievementsForm_achievement_rewards').val(sliderText);
			$('#navbar').html("<?=$place?> in "+sliderText);

			if (x == 0){
				$('#navbar').html("Present day <?=$place?>");
			}

			changeColor(x*100);
			
			var width = x*100;
			var opacity = x;
			var opacityBg = x*0.5;
			var opacityBgInv = 0.5-opacityBg;
			$('#just-a-slider > .hp-bar-container').css('width', width+'%');
			$('#just-a-slider > .hp-bar-container > .hp-bar').css('background-color', 'rgba(217, 30, 24, '+opacityBg+')'); //INSIDE
			$('#just-a-slider > .hp-bar-container').css('background-color', 'rgba(38, 166, 91, '+(opacityBgInv)+')'); //OUTSIDE
			$('#just-a-slider > .handle > .slider').css('background-color', 'rgba(217, 30, 24, '+opacity+')'); //INSIDE
			
			if(x >= 0.5){
				$('#just-a-slider > .slider-text').css('text-align', 'left');
			
			} else {
				$('#just-a-slider > .slider-text').css('text-align', 'right');
			}
			// $('#just-a-slider .value').text(((x*900)+100).toFixed(2));
		  }
		});
			
		rewardDealer.setValue(0, 0, true);
	});

</script>

<div class="stats">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<tr>
					<th>Carbon Footprint</th>
					<td>N/A</td>
				</tr>
				<tr>
					<th>Population</th>
					<td>N/A</td>
				</tr>
				<tr>
					<th>Water Pollution</th>
					<td>N/A</td>
				</tr>
				<tr>
					<th>Air Pollution</th>
					<td>N/A</td>
				</tr>
				<tr>
					<th>Forest Cover</th>
					<td>N/A</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div class="map">
</div>
<img class="map-image" id="map" onload="getPixels(this);" src="/htc-aninag/images/<?=$place?>.png"/>
<div class="yay"></div>
<div class="gitna">
</div>
<div class="gitna-description">
</div>
<div class="big-round-button" id="a" type="1"><img src="/htc-aninag/images/airpollution.png"/></div>
<div class="big-round-button" id="b" type="2"><img src="/htc-aninag/images/footprint.png"/></div>
<div class="big-round-button" id="c" type="3"><img src="/htc-aninag/images/population.png"/></div>
<div class="big-round-button" id="d" type="4"><img src="/htc-aninag/images/tree.png"/></div>
<div class="big-round-button" id="e" type="5"><img src="/htc-aninag/images/water.png"/></div>
<div class="big-round-button" id="f" type="6"><img src="/htc-aninag/images/animal.png"/></div>

<br/>
<br/>
<br/>
<br/>
<div id="just-a-slider" class="dragdealer dragger">
	<div class="hp-bar-container">
		<div class="hp-bar"></div>
	</div>
	<div class="handle drag-bar slider-bg">
		<div class="slider value"></div>
	</div>
</div>