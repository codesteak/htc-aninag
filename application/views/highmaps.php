<script type="text/javascript" src="/htc-aninag/javascripts/jquery-1.9.1.js"></script>
<script type="text/javascript" src="/htc-aninag/javascripts/vendor/highmaps.js"></script>
<script src="http://code.highcharts.com/mapdata/index.js?1"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>

<script src="http://www.highcharts.com/samples/maps/demo/all-maps/jquery.combobox.js"></script>
<script type="text/javascript" src="/htc-aninag/javascripts/highmaps.js"></script>

<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet">
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/htc-aninag/stylesheets/highmaps.css">

<style>

</style>

<br/>
<br/>
<br/>

<center id="spinner" style="margin-top: 120px;">
  <i class="fa fa-spinner fa-spin" style="font-size: 100px;"></i>
  <br/>
  <span class="cursive" style="font-size: 24px;">
    Gathering the latest environmental data ...
  </span>
</center>
<div id="demo-wrapper">
    <div id="mapBox" style="padding-top: 10px;>
        <div id="relative" style="position: absolute;">
            <div id="up"></div>
        </div>
        <div style="clear: both;"></div>
        <br/>
        <div class="selector" style="display: none;">
            <button id="btn-prev-map" class="prev-next"><i class="fa fa-angle-left"></i></button>
            <select id="mapDropdown" class="ui-widget combobox"></select>
            <button id="btn-next-map" class="prev-next"><i class="fa fa-angle-right"></i></button>
        </div>
        <div id="container"></div> 
    </div>
    <div id="sideBox" style="display: none;">
        <input type="checkbox" id="chkDataLabels" checked='checked' />
        <label for="chkDataLabels" style="display: inline">Data LALALAabels</label>
        <div id="infoBox">
            <h4>This map</h4>
            <div id="download"></div>
        </div>
    </div>

    <form id="gmaps_snapshot_form" method="post" action="/htc-aninag/Aninag/gmaps_snapshot">
        <input id="gmaps_city" type="hidden" name="city" value="" /> 
    </form>
</div>
