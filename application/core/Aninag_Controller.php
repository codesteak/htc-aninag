<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* \brief Extension of CodeIgniter's Controller class.
*
* Is used to create a main template and a content template that varies for each module.
*/
class Aninag_Controller extends CI_Controller  {

	private $_messages;

	/**
	* \brief A protected variable.
	*
	* Contains path to the main template (a view).
	*/
	protected $layout;

	/**
	* \brief a protected variable
	*
	* The title of the page
	*/
	protected $page_title;
	/**
	* \brief The class constructor.
	*
	* Calls the parent constructor and assigns values to protected variables.
	* Loads Layer 3 Models implicitly.
	*/
	
	public function __construct() {
		parent::__construct();	
		
		$this->layout = 'layout';
	}

	/**
	* \brief An alternative to Load::view()
	*
	* Functions the same as the Load::view() function if the protected variable $layout is set to null.
	* Otherwise, it displays the main template with the content template, which is interchangeable, at the center.
	*
	* \param view the path of the content template
	* \param vars a container of variables that are passed to the templates/views
	* \param return a boolean value. If set to true, does not display template and returns a value instead
	* \sa Load::view()
	*/
	protected function load_view($view, $vars = array()) {
		// return $this->load->view($view, $vars, $return);
		return $this->load->view($this->layout, array('view' => $view, 'vars' => $vars));
	}

}
